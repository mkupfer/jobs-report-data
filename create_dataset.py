import os

import bls

SERIES = {
    'Total Nonfarm Employment': 'CES0000000001',
    'Private Employment': 'CES0500000001',
    'N. Resources/mining': 'CES1000000001',
    'Construction': 'CES2000000001',
    'Manufacturing': 'CES3000000001',
    'Wholesale Trade': 'CES4142000001',
    'Retail Trade': 'CES4200000001',
    'Information': 'CES5000000001',
    'Financial Activities': 'CES5500000001',
    'Prof. Serv.': 'CES6000000001',
    'Temporary Help': 'CES6056132001',
    'Education': 'CES6561000001',
    'Health and social assistance': 'CES6562000001',
    'Leisure and hospitality': 'CES7000000001',
    'Other Services': 'CES8000000001',
    'Government': 'CES9000000001',
    'Average weekly hours of all employees': 'CES0500000002',
    'Average weekly Overtime (Mfg)': 'CES3000000009',
    'Transp. & Ware': 'CES4300000001',
    'Unemployment Level': 'LNS13000000',
    'Civilian Labor Force': 'LNS11000000',
    'Employment level': 'LNS12000000',
    'College Graduate Unemployment Rate': 'LNS14027662',
    'Civilian labor force level College Grad': 'LNS11027662',
    'Employment level College Grad': 'LNS12027662',
    'Unemployment Level College Grad': 'LNS13027662',
    'Labor Force Participation Rate': 'LNS11300000',
    'Job Openings': 'JTS00000000JOL',
    'Job Openings as a Percent of Total Employment': 'JTS00000000JOR',
    'Hires': 'JTS00000000HIL',
    'Hires as a Percent of Total Employment': 'JTS00000000HIR',
    'Quits': 'JTS00000000QUL',
    'Quits as a Percent of Total Employment': 'JTS00000000QUR',
    'Involuntary Separations': 'JTS00000000LDL',
    'Involuntary Separations as a Percent of Total Employment': 'JTS00000000LDR',
    'Executive Search': 'CEU6056131201',
    'Direct Hire': 'CEU6056131101',
    'PEO Employees': 'CEU6056133001',
}

def remove_overlap_concat(dfs):
    dfs[0].index.to_list()

def main():
    series_ids = [v for v in SERIES.values()]
    df_last_20 = bls.get_series(series_ids)
    df_long_term = bls.get_series(series=[
        SERIES['Total Nonfarm Employment'],
        SERIES['Temporary Help']], startyear=1990)

    # df_long_term will overlap with df_last_20 so we use combine_first to
    # ignore duplicates, but add in the older data.
    non_overlapping_merge = df_last_20.combine_first(df_long_term)
    non_overlapping_merge.to_pickle('data_set.pkl')


if __name__ == "__main__":
    try:
        os.environ.get('BLS_API_KEY')
    except KeyError:
        print('No BLS_API_KEY set in environment variables.\
              \nSet BLS_API_KEY and rerun')
    main()
